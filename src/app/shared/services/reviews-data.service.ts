import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Review } from './../models/review.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewsDataService {

  constructor(
    private http: HttpClient
  ) { }

  private reviewsUrl = 'api/reviews';

  getAllReviews(): Observable<Review[]>{
    return this.http.get<Review[]>(this.reviewsUrl);
  }
}
