import { Message } from './../models/message.model';
import { User } from './../models/user.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient) { }

  private usersUrl = 'api/users';
  private massagesUrl = 'api/messages';

  private addMessageSource = new BehaviorSubject<Message>({
    user: {
      id: 0,
      name: '',
      information: '',
      avatarUrl: '',
      isAdmin: false
    },
    text: '',
    date: new Date()

  });
  messageAdding = this.addMessageSource.asObservable();

  getUser(id: number): Observable<User[]> {
    const params: HttpParams = new HttpParams().set('id', id.toString());
    const options = {params};
    return this.http.get<User[]>(this.usersUrl, options);
  }

  getAllMessages(): Observable<Message[]> {
    return this.http.get<Message[]>(this.massagesUrl);
  }

  addMessage(message: Message): Observable<Message> {
    const options = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    console.log(message);
    return this.http.post<Message>(this.massagesUrl, message, options);
  }

  emitAddMessage(message: Message) {
    this.addMessageSource.next(message);
  }
}
