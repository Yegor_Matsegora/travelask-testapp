import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { City } from './../models/city.model';

@Injectable({
  providedIn: 'root'
})
export class CitysDataService {

  constructor(private http: HttpClient) { }

  private cityUrl = 'api/citys';

  getCity(id: number): Observable<City[]> {
    const params: HttpParams = new HttpParams().set('id', id.toString());
    const options = {params};
    return this.http.get<City[]>(
      this.cityUrl, options
    );
  }
}
