import { Message } from './../models/message.model';
import { User } from './../models/user.model';
import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

import { City } from '../models/city.model';
import { Review } from './../models/review.model';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const citys: City[] = [
      {
        id: 1,
        name: 'Барселона',
        heading: 'Барселона — обзор города',
        // tslint:disable-next-line: max-line-length
        description: 'Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        imgUrl: './assets/images/barcelona.jpg'
      }
    ];

    const reviews: Review[] = [
      {
        id: 1,
        city: 'Барселона',
        likes: 9,
        comments: 9,
        // tslint:disable-next-line: max-line-length
        text: 'Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором ',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Наталья Полянская',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 2,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором ',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 3,
        city: 'Барселона',
        likes: 9,
        comments: 9,
        // tslint:disable-next-line: max-line-length
        text: 'Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Наталья Полянская',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 4,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 5,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 6,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 7,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 8,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 9,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 10,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 11,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 12,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 13,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }, {
        id: 14,
        city: 'Барселона',
        likes: 3,
        comments: 7,
        // tslint:disable-next-line: max-line-length
        text: 'Плюсы города: весь город одни плюсы! Минусы города: не видела. В наш марафон по Европе не вписалось 2 испанских города от усталости - решили остаток путешествия провести в Барселоне Барселона – моя третья большая любовь, после Вены и Крита. Это город, в который я каждый раз возвращаюсь с огромным удовольствием, всем рекомендую хоть раз там побывать и осмотреть Барселона с её золотистыми пляжами, архитектурными шедеврами Гауди, многочисленными фестивалями и гастрономическим разнообразием понравилась мне в первый же день пребывания и стала местом, в котором',
        date: new Date(2017, 10, 5, 10, 10),
        user: {
          name: 'Elena Bulgakova',
          imgUrl: './assets/images/userimg1.jpg'
        },
        fotos: [
          './assets/images/IMG_2299.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png',
          './assets/images/IMG_22992.png',
          './assets/images/IMG_2388.png',
          './assets/images/IMG_2675.png'
        ]
      }
    ];

    const users: User[] = [
      {
        id: 1,
        name: 'Наталья Полянская',
        information: 'Гид по баварии, фотограф',
        avatarUrl: './assets/images/userimg1.jpg',
        isAdmin: false
      }, {
        id: 2,
        name: 'Elena Bulgakova',
        information: 'TravelAsk',
        avatarUrl: './assets/images/userimg2.png',
        isAdmin: true
      }
    ];

    const messages: Message[] = [
      {
        user: {
          id: 1,
          name: 'Наталья Полянская',
          information: 'Гид по баварии, фотограф',
          avatarUrl: './assets/images/userimg1.jpg',
          isAdmin: false
        },
        // tslint:disable-next-line: max-line-length
        text: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis accusantium ab ex qui incidunt provident quis! Esse consequuntur vel illum.',
        date: new Date(2019, 11, 18, 10, 10),
        id: 1
      }, {
        user: {
          id: 2,
          name: 'Elena Bulgakova',
          information: 'TravelAsk',
          avatarUrl: './assets/images/userimg2.png',
          isAdmin: true
        },
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est obcaecati nobis aliquam et, nihil sunt vitae culpa odit alias? Quia veniam atque qui corrupti quisquam harum unde magnam accusamus consequuntur!',
      id: 2,
      date: new Date(2019, 11, 10, 10, 10),
      }, {
        user: {
          id: 1,
          name: 'Наталья Полянская',
          information: 'Гид по баварии, фотограф',
          avatarUrl: './assets/images/userimg1.jpg',
          isAdmin: false
        },
        // tslint:disable-next-line: max-line-length
        text: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis accusantium ab ex qui incidunt provident quis! Esse consequuntur vel illum.',

        date: new Date(2019, 8, 10, 10, 10),
        id: 3
      }, {
        user: {
          id: 2,
          name: 'Elena Bulgakova',
          information: 'TravelAsk',
          avatarUrl: './assets/images/userimg2.png',
          isAdmin: true
        },
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est obcaecati nobis aliquam et, nihil sunt vitae culpa odit alias? Quia veniam atque qui corrupti quisquam harum unde magnam accusamus consequuntur!',
      id: 4,
      date: new Date(2018, 8, 10, 10, 10),
      }
    ];

    return {citys, reviews, users, messages};
  }

  genId(items): number {
    return items.length > 0 ? Math.max(...items.map(item => item.id)) + 1 : 1;
  }
}
