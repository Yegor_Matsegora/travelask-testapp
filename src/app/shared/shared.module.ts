import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentSliderComponent } from './content-slider/content-slider.component';
import { SlideDirective } from './content-slider/slide.directive';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfigDatePipe } from './config-date.pipe';



@NgModule({
  declarations: [
    ContentSliderComponent,
    SlideDirective,
    ConfigDatePipe
  ],
  imports: [
    CommonModule,
    NgbCarouselModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    ContentSliderComponent,
    SlideDirective,
    NgbCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    ConfigDatePipe
  ]
})
export class SharedModule { }
