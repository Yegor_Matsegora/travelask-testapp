import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'configDate'
})
export class ConfigDatePipe implements PipeTransform {

  transform(value): string {
    const dateNow: Date = new Date();
    value = new Date(value);

    if (value.getFullYear() - dateNow.getFullYear() > 1 && value.getMonth() < dateNow.getMonth()) {
      return `более ${value.getUTCFullYear() - dateNow.getUTCFullYear()} лет назад`;
    } else {
      if (value.getUTCFullYear() - dateNow.getUTCFullYear() === 1) {
        if (value.getMonth() < dateNow.getMonth()) {
          return `более 1 года назад`;
        }
        if (dateNow.getMonth() === value.getMonth()) {
          return `около 1 года назад`;
        }
      } else {
        if (value.getMonth() < dateNow.getMonth()) {
          if (value.getUTCDate() <= dateNow.getUTCDate()) {
            return `${(dateNow.getMonth() + 1) - (value.getMonth() + 1)} месяцев(а) назад`;
          } else {
            return `${((dateNow.getMonth() + 1) - (value.getMonth() + 1) + 1)} месяцев(а) назад`;
          }
        } else {
          if (value.getDate() < dateNow.getDate()) {
            if (dateNow.getDate() - value.getDate() === 1) {
              return `вчера в ${value.getHours()}:${value.getMinutes()}`;
            } else {
              return `${dateNow.getDate() - value.getDate()} дней(я) назад`;
            }
          } else {
            if (value.getMinutes() < 10) {
            return `сегодня в ${value.getHours()}:0${value.getMinutes()}`;
            } else {
              return `сегодня в ${value.getHours()}:${value.getMinutes()}`;
            }
          }
        }
      }
    }
  }

}
