import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[app-slide]'
})
export class SlideDirective {

  @HostBinding('style.display') display: string;

  constructor() {
    this.display = 'inline-block';
  }

}
