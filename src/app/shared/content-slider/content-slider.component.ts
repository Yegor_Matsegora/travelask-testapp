import {
  Component,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  AfterViewInit,
  Renderer2
} from "@angular/core";

interface Slide {
  id: number;
  position: number;
}

@Component({
  selector: "app-content-slider",
  templateUrl: "./content-slider.component.html",
  styleUrls: ["./content-slider.component.scss"]
})
export class ContentSliderComponent implements AfterViewChecked, AfterViewInit {
  @ViewChild("slidesContainer", { static: false }) slidesContainer: ElementRef;
  @ViewChild("slider", { static: true }) slider: ElementRef;
  sliderWidth: number;
  allSlidesWidth: number;
  visibleSlidesWidth: number;
  activeSlideId = 0;
  allSlides: Slide[] = [];
  isInStart = true;
  isInEnd = false;
  isEnoughSlides = true;

  currentPosition = 0;

  constructor(private renderer: Renderer2) {}

  ngAfterViewInit() {
    this.slidesContainer.nativeElement.style.left = "0";
  }

  ngAfterViewChecked() {
    this.initSlider();
  }

  initSlider() {
    this.allSlidesWidth = this.slidesContainer.nativeElement.offsetWidth;
    this.sliderWidth = this.slider.nativeElement.offsetWidth;
    const slides: [] = this.slidesContainer.nativeElement.children;
    let currentSlidesWidth = 0;
    let firstSlideCoordinate: number;

    if (slides.length < 2) {
      this.visibleSlidesWidth = this.sliderWidth;
    } else {
      for (let i = 0; i < slides.length; i++) {
        const c: any = slides[i];

        if (i === 0) {
          firstSlideCoordinate = c.getBoundingClientRect().x;
          this.visibleSlidesWidth =
            c.nextElementSibling.getBoundingClientRect().x -
            firstSlideCoordinate;
        }
        currentSlidesWidth = c.getBoundingClientRect().x - firstSlideCoordinate;
        if (currentSlidesWidth < this.sliderWidth) {
          this.visibleSlidesWidth = currentSlidesWidth;
        } else {
          break;
        }
      }
    }

    let slidesNumber = 0;

    if (this.allSlidesWidth % this.visibleSlidesWidth === 0) {
      slidesNumber = this.allSlidesWidth / +this.visibleSlidesWidth;
    } else {
      slidesNumber =
        Math.floor(this.allSlidesWidth / +this.visibleSlidesWidth) + 1;
    }

    let position = 0;
    this.allSlides.length = 0;

    for (let i = 0; i < slidesNumber; i++) {
      this.allSlides.push({ id: i, position });
      if (
        position + this.visibleSlidesWidth <
        this.allSlidesWidth - this.sliderWidth
      ) {
        position += this.visibleSlidesWidth;
      } else {
        position = this.allSlidesWidth - this.sliderWidth;
      }
    }
  }

  checkControls() {
    switch (this.activeSlideId) {
      case 0:
        this.isInStart = true;
        this.isInEnd = false;
        break;
      case this.allSlides.length - 1:
        this.isInStart = false;
        this.isInEnd = true;
        break;
      default:
        this.isInEnd = false;
        this.isInStart = false;
    }
  }

  moveSlide() {
    this.renderer.setStyle(
      this.slidesContainer.nativeElement,
      "transitionDuration",
      "200ms"
    );
    this.renderer.setStyle(
      this.slidesContainer.nativeElement,
      "transitionProperty",
      "all"
    );
    this.renderer.setStyle(
      this.slidesContainer.nativeElement,
      "left",
      "`-${this.allSlides[this.activeSlideId].position}px`"
    );
  }

  slideNext() {
    this.activeSlideId++;
    this.checkControls();
    if (this.activeSlideId + 1 === this.allSlides.length) {
      this.activeSlideId = this.allSlides.length - 1;
    }
    this.moveSlide();
  }

  slidePrev() {
    if (this.activeSlideId === 0) {
      return;
    }
    this.activeSlideId--;
    this.moveSlide();
    this.checkControls();
  }

  moveToSlide(id: number) {
    this.activeSlideId = id;
    this.moveSlide();
    this.checkControls();
  }
}
