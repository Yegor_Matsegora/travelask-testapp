export class User {
  id?: number;
  name: string;
  avatarUrl: string;
  information: string;
  isAdmin: boolean;
}
