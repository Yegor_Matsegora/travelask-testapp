export class Review {
  id?: number;
  user: {
    name: string;
    imgUrl?: string;
  };
  city: string;
  text: string;
  likes: number;
  date: any;
  comments: number;
  fotos: string[];
}
