export class City {
    public name: string;
    public heading: string;
    public description: string;
    public imgUrl: string;
    public id?: number;
}
