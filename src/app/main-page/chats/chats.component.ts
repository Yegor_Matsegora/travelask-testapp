import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { Message } from './../../shared/models/message.model';
import { ChatService } from './../../shared/services/chat.service';
import { User } from './../../shared/models/user.model';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit {

  constructor( private chatService: ChatService ) { }

  @Input() userId: number;

  user: User = {
    name: '',
    avatarUrl: '',
    information: '',
    isAdmin: false
  };
  messages: Message[] = [];
  messageControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(300)
  ]);

  ngOnInit() {
    this.chatService.getUser(this.userId)
      .subscribe( (users) => {
        const u: User = users[0];
        this.user = Object.assign({}, u);
      });
    this.chatService.getAllMessages()
      .subscribe( (messages) => {
        this.messages = messages;
      });
    this.chatService.messageAdding
      .subscribe( (message: Message) => {
        if (message.text.length) {
          this.messages.unshift(message);
        }
      });
  }

  sendMessage() {
    const u = Object.assign({}, this.user);
    const m: Message = {
      user: u,
      text: this.messageControl.value,
      date: new Date()
    };
    this.chatService.addMessage(m)
      .subscribe((data) => {
        this.chatService.emitAddMessage(m);
        this.messageControl.reset();
      });
  }

}
