import { AppRoutingModule } from './../app-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from './../shared/shared.module';
import { MainPageComponent } from './main-page.component';
import { AboutCityComponent } from './about-city/about-city.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { ChatsComponent } from './chats/chats.component';
import { ReviewDescComponent } from './reviews/review-desc/review-desc.component';
import { ReviewFotosPopupComponent } from './reviews/review-fotos-popup/review-fotos-popup.component';


@NgModule({
  declarations: [
    MainPageComponent,
    AboutCityComponent,
    ReviewsComponent,
    ChatsComponent,
    ReviewDescComponent,
    ReviewFotosPopupComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule
  ]
})
export class MainPageModule { }
