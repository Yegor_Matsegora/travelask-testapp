import { City } from './../../shared/models/city.model';
import { CitysDataService } from '../../shared/services/citys-data.service';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-about-city',
  templateUrl: './about-city.component.html',
  styleUrls: ['./about-city.component.scss']
})
export class AboutCityComponent implements OnInit {

  constructor(
    private citysDataService: CitysDataService
  ) { }

  city: City = {
    name: '',
    heading: '',
    description: '',
    imgUrl: ''
  };
  cityId = 1;

  ngOnInit() {
    this.citysDataService.getCity(this.cityId)
      .subscribe((citys) => {
        const c = citys[0];
        this.city = Object.assign({}, c);
      });
  }
}
