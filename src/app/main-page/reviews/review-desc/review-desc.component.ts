import { Component, OnInit, Input } from '@angular/core';
import { Review } from 'src/app/shared/models/review.model';

@Component({
  selector: 'app-review-desc',
  templateUrl: './review-desc.component.html',
  styleUrls: ['./review-desc.component.scss']
})
export class ReviewDescComponent implements OnInit {

  constructor() { }

  @Input() review: Review;
  isPopupVisible = false;

  ngOnInit() {
  }

  showPopup() {
    this.isPopupVisible = true;
  }

  closePopup() {
    this.isPopupVisible = false;
  }
}
