import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-review-fotos-popup',
  templateUrl: './review-fotos-popup.component.html',
  styleUrls: ['./review-fotos-popup.component.scss']
})
export class ReviewFotosPopupComponent implements AfterViewInit {

  @Input() fotos: string[];
  @ViewChild('carousel', {static : false}) carousel: NgbCarousel;
  @Output() isConfirmed: EventEmitter<any> = new EventEmitter();

  ngAfterViewInit() {
    this.carousel.pause();
  }

  close() {
    this.isConfirmed.emit();
  }

}
