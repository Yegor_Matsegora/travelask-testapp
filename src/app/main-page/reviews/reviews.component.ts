import { ReviewsDataService } from './../../shared/services/reviews-data.service';
import { Review } from './../../shared/models/review.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {

  constructor(
    private reviewsDataService: ReviewsDataService
  ) { }

  reviews: Review[];

  ngOnInit() {
    this.reviewsDataService.getAllReviews()
      .subscribe(reviews => {
        this.reviews = reviews;
      });
  }

}
